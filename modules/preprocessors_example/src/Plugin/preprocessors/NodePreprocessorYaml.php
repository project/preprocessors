<?php

namespace Drupal\preprocessors_example\Plugin\preprocessors;

use Drupal\preprocessors\PreprocessorPluginBase;

/**
 * Provide plugin to alter variables for nodes.
 *
 * This is an added example that is discovered through Yaml Discovery!
 *
 * @package Drupal\preprocessors_example\Plugin\preprocessors
 */
final class NodePreprocessorYaml extends PreprocessorPluginBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, string $hook, array $info) : void {
    $variables['foo'] = 'bar';
  }

}
