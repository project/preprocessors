<?php

namespace Drupal\preprocessors_examples\Plugin\preprocessors;

use Drupal\preprocessors\PreprocessorPluginBase;

/**
 * Provide plugin to alter variables for "page" type nodes.
 *
 * @Preprocessor(
 *   id = "preprocessors_example.preprocessor.page",
 *   hooks = {
 *     "node__page"
 *   },
 *   themes = '*',
 *   weight = 0,
 * )
 *
 * @package Drupal\preprocessors_example\Plugin\preprocessors
 */
final class PagePreprocessor extends PreprocessorPluginBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, string $hook, array $info) : void {
    $variables['foo'] = 'bar';
  }

}
