<?php

namespace Drupal\preprocessors_example\Plugin\preprocessors;

use Drupal\preprocessors\PreprocessorPluginBase;

/**
 * Provide plugin to alter variables for nodes.
 *
 * @Preprocessor(
 *   id = "preprocessors_example.preprocessor.node",
 *   hooks = {
 *     "node"
 *   },
 *   themes = '*',
 *   weight = 0,
 * )
 *
 * @package Drupal\preprocessors_example\Plugin\preprocessors
 */
final class NodePreprocessor extends PreprocessorPluginBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, string $hook, array $info) : void {
    $variables['foo'] = 'bar';
  }

}
