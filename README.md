# Preprocessor Plugins

The Preprocessor Plugins module grants developers the ability to create Plugins to
preprocess template variables instead of traditional Preprocess Functions.

- [Theme Plugins](#using-preprocessor-plugins-in-themes)
- [Module Plugins](#using-preprocessor-plugins-in-modules)
- [Plugin Properties](#plugin-properties)
- [Technical Specs](#technical-specifications)
  - [Preprocessor Execution Flow](#execution-order)
  - [Weight](#weight)

This module serves two main purposes:

* Align preprocessing with more modern Drupal practices, allowing a more OOP approach and granting the ability to use Dependency Injection and more.
* Prevent large .theme files that contain all preprocessing logic. Large files are easy to get lost in and are less organized.

## Using Preprocessor Plugins In Themes

Traditionally, you would preprocess a node template using the following code:

```php
/**
 * Implements hook_preprocess_HOOK().
 */
function MY_THEME_preprocess_node(&$variables) {
  $variables['foo'] = 'bar';
}
```

To achieve the same functionality as above within your **theme** with a
**Plugin**, you would do the following:

[Plugins can only be discovered in themes via YAML discovery](https://www.drupal.org/docs/8/api/plugin-api/creating-a-plugin-that-can-be-defined-in-themes).
So to start, create a `MY_THEME.preprocessors.yml` file at the root of your theme.

```yaml
MY_THEME.preprocessor.node:
  class: '\Drupal\MY_THEME\Plugin\preprocessors\NodePreprocessor'
  hooks:
    - node
  weight: 0
```

Following this, create a class at
`MY_THEME/src/Plugin/preprocessors/NodePreprocessor.php` with the following content:

```php
<?php

namespace Drupal\MY_THEME\Plugin\preprocessors;

use Drupal\preprocessors\PreprocessorPluginBase;

/**
 * Provide plugin to preprocess variables for nodes.
 */
final class NodePreprocessor extends PreprocessorPluginBase {

  /**
   * Preprocess your variables.
   *
   * This function works just like a 'hook_preprocess_HOOK()' function.
   *
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, string $hook, array $info) : void {
    $variables['foo'] = 'bar';
  }

}
```

## Using Preprocessor Plugins In Modules

In modules, you can make use of YAML discovery as well.
This means that by following the exact same steps as above, you can create
Plugins in your modules.

An alternative and more traditional way of creating Plugins in modules
is the use of [Annotation-Based Plugins](https://www.drupal.org/docs/drupal-apis/plugin-api/annotations-based-plugins).

Using the same example to preprocess a `node.html.twig` template, you can create a class at
`MY_MODULE/src/Plugin/preprocessors/NodePreprocessor.php` with the following
content:

```php
<?php

namespace Drupal\MY_MODULE\Plugin\preprocessors;

use Drupal\preprocessors\PreprocessorPluginBase;

/**
 * Provide plugin to preprocess variables for nodes.
 *
 * @Preprocessor(
 *   id = "MY_MODULE.preprocessor.node",
 *   hooks = {
 *     "node",
 *   },
 *   themes = "*",
 *   weight = 0,
 * )
 */
final class NodePreprocessor extends PreprocessorPluginBase {

  /**
   * Preprocess your variables.
   *
   * This function works just like a 'hook_preprocess_HOOK()' function.
   *
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, string $hook, array $info) : void {
    $variables['foo'] = "bar";
  }

}
```

All properties of the plugin are set in the `@Preprocessor` annotation in the
class' doc comment.

## Plugin Properties

* `hooks` - The hooks this plugin should act upon.
  * Plugins can act on multiple hooks if desired, hence this value being an array.
  * The values here are the same you would replace HOOK with in a hook_preprocess_HOOK function.
* `themes` **(Module Plugins Only)** - Designate which themes this plugin should act on.
  * This is only useful in modules when you want to limit which themes your module plugins affect. Plugins defined in themes only act upon the themes they are defined in.
* `weight` - Determines the weight of the plugin.
  * This affects execution order relative to other plugins affecting the same hooks.

## Technical Specifications

### Execution Order

It's important to discuss how these preprocess plugins behave in the presence of
traditional Drupal preprocess hooks.

The module is designed to execute plugin preprocessing in a way that makes sense
relative to how Drupal does it normally. To elaborate:

If you have existing preprocess hooks in modules **and** in themes, Drupal will:

* Preprocess templates with module hooks for the current template's base hook, if any.
* Preprocess templates with theme hooks for the current template's base hook, if any.
* Preprocess templates with module hooks for the current template's hook.
* Preprocess templates with theme hooks for the current template's hook.

The important thing to note is that base hook preprocessing happens first if
there is a base hook for a template. Then, preprocessing happens for the hook
itself.

Preprocessor Plugins attempt to simulate this in a way that makes sense. With
the module enabled, you can expect the following flow of execution:

* Preprocess templates with module hooks for the current template's base hook, if any.
* Preprocess templates with module preprocessor plugins for the current template's base hook, if any.
* Preprocess templates with theme preprocessor plugins for the current template's base hook, if any.
* Preprocess templates with theme hooks for the current template's base hook, if any.
* Preprocess templates with module hooks for the current template's hook.
* Preprocess templates with module preprocessor plugins for the current template's hook.
* Preprocess templates with theme preprocessor plugins for the current template's hook.
* Preprocess templates with theme hooks for the current template's hook.

Things to consider for the above:

* Preprocessor Plugins from **Modules** will act **after** traditional **module** hooks.
* Preprocessor Plugins from **Themes** will act **before** traditional **theme** hooks.
* In other words, plugins act between the preprocessing layer of modules and themes.
* Base hook preprocessing will always happen beforehand, as it is in core.

### Weight

Because of what was just discussed above, the **Weight** property of the plugins
will only sort by plugins that alter the same hook. This is to maintain the
strict flow we want, all while giving devs the possibility to control which
plugin acts first when two plugins act upon the same hook(s).
