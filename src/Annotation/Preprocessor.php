<?php

namespace Drupal\preprocessors\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Preprocessor item annotation object.
 *
 * Plugin Namespace: Plugin\preprocessors.
 *
 * @see plugin_api
 *
 * @package Drupal\preprocessors\Annotation
 *
 * @Annotation
 */
final class Preprocessor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The target theme hook suggestions to preprocess variables for.
   *
   * @var array
   */
  protected array $hooks;

  /**
   * The themes to preprocess variables in.
   *
   * @var array|string
   */
  protected array|string $themes;

  /**
   * The weight of this preprocessor.
   *
   * @var int
   */
  protected int $weight;

}
