<?php

namespace Drupal\preprocessors;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for Preprocessor plugins.
 *
 * @package Drupal\preprocessors
 */
interface PreprocessorInterface extends PluginInspectionInterface {

  /**
   * Return the ID of the preprocessor.
   *
   * @return string
   *   Return the ID of the Preprocessor.
   */
  public function getId(): string;

  /**
   * Return the provider of the preprocessor.
   *
   * @return string
   *   Return the provider of the Preprocessor.
   */
  public function getProvider(): string;

  /**
   * Return the theme hook of the Preprocessor.
   *
   * @return string
   *   Return the theme hook that this preprocessor is applying alterations to.
   */
  public function getHooks(): string;

  /**
   * Return the themes this Preprocessor will be applied in.
   *
   * @return array|string
   *   Return the themes that the preprocessor should apply alterations in.
   *   Will return '*' when applying to all themes.
   */
  public function getThemes(): array|string;

  /**
   * Return the weight of the Preprocessor.
   *
   * @return int
   *   Return the weight of this preprocessor.
   */
  public function getWeight(): int;

  /**
   * Alter the data provided to the Preprocessor.
   *
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21theme.api.php/function/hook_preprocess_HOOK/10
   */
  public function preprocess(array &$variables, string $hook, array $info);

}
