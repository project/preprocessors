<?php

namespace Drupal\preprocessors;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface definition for managing preprocessor plugins.
 *
 * @package Drupal\preprocessors
 */
interface PreprocessorsPluginManagerInterface extends PluginManagerInterface {

  /**
   * Get preprocessors for a given hook.
   *
   * @return \Drupal\preprocessors\PreprocessorInterface[]
   *   Array of preprocessors.
   */
  public function getPreprocessors(): array;

  /**
   * Checks whether there are any preprocessors for the request at that time.
   *
   * @return bool
   *   Whether there are preprocessors.
   */
  public function hasPreprocessors(): bool;

}
