<?php

namespace Drupal\preprocessors;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\Registry;
use Drupal\Core\Theme\ThemeManagerInterface;

/**
 * Provides a service for Preprocessor Files.
 */
final class Preprocessors {
  use StringTranslationTrait;

  /**
   * Use DI to inject Drupal's configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public ConfigFactoryInterface $configFactory;

  /**
   * The Module Handler injected through DI.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Theme Manager injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  private $themeManager;

  /**
   * The Theme Registry injected through DI.
   *
   * @var \Drupal\core\Theme\Registry
   */
  private $themeRegistry;

  /**
   * Preprocessor Plugin Manager injected through DI.
   *
   * @var \Drupal\preprocessors\PreprocessorsPluginManager
   */
  private $preprocessorPluginManager;

  /**
   * PreprocessorsManager constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme Manager injected through DI.
   * @param \Drupal\Core\Theme\Registry $themeRegistry
   *   The theme registry.
   * @param \Drupal\preprocessors\PreprocessorsPluginManager $preprocessorPluginManager
   *   Preprocessor Plugin Manager injected through DI.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, ThemeManagerInterface $themeManager, Registry $themeRegistry, PreprocessorsPluginManager $preprocessorPluginManager) {
    $this->moduleHandler = $moduleHandler;
    $this->themeManager = $themeManager;
    $this->themeRegistry = $themeRegistry;
    $this->preprocessorPluginManager = $preprocessorPluginManager;
  }

  /**
   * Shortcut to obtain the Preprocessors service from the global container.
   *
   * @return \Drupal\preprocessors\Preprocessors
   *   The PreprocessorFiles service.
   */
  public static function service() : Preprocessors {
    static $service;
    if (!empty($service)) {
      return $service;
    }
    $service = \Drupal::service('preprocessors');
    return $service;
  }

  /**
   * Perform preprocessing with a given array of plugins.
   *
   * @param array $plugins
   *   Plugins array computed by the module.
   * @param array $variables
   *   Variables to preprocess.
   * @param string $hook
   *   Template hook that is being preprocessed.
   * @param array $info
   *   Template info array.
   */
  public static function preprocess(array $plugins, array &$variables, string $hook, array $info) : void {
    $service = self::service();
    $modulePlugins = $plugins['module'] ?? [];
    $themePlugins = $plugins['theme'] ?? [];
    foreach ($modulePlugins as $id) {
      $preprocessor = $service->getPreprocessorPluginById($id);
      if (!empty($preprocessor)) {
        $preprocessor->preprocess($variables, $hook, $info);
      }
    }
    foreach ($themePlugins as $id) {
      $preprocessor = $service->getPreprocessorPluginById($id);
      if (!empty($preprocessor)) {
        $preprocessor->preprocess($variables, $hook, $info);
      }
    }
  }

  /**
   * Return the list of modules.
   *
   * @return array
   *   The list of modules.
   */
  public function getModuleList() : array {
    return $this->moduleHandler->getModuleList();
  }

  /**
   * Get all preprocessor plugins.
   *
   * @return \Drupal\preprocessors\PreprocessorInterface[]
   *   The discovered preprocessor plugins.
   */
  public function getPreprocessorPlugins() : array {
    return $this->preprocessorPluginManager->getPreprocessors();
  }

  /**
   * Get a preprocessor plugin by ID.
   *
   * @param string $id
   *   ID of the plugin to get.
   *
   * @return PreprocessorInterface|null
   *   The preprocessor if found.
   */
  public function getPreprocessorPluginById(string $id) : ?object {
    try {
      $plugin = $this->preprocessorPluginManager->createInstance($id);
    }
    catch (PluginException $e) {
      return NULL;
    }
    return $plugin;
  }

}
