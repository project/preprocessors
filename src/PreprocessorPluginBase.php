<?php

namespace Drupal\preprocessors;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for Preprocessor plugins.
 *
 * @package Drupal\preprocessors
 */
abstract class PreprocessorPluginBase extends PluginBase implements PreprocessorInterface, ContainerFactoryPluginInterface {

  /**
   * Constant for identifier for preprocessors that apply to all themes.
   *
   * @var string
   */
  public const ALL_THEMES_VALUE = '*';

  /**
   * Constant for the id property of a Preprocessor definition.
   *
   * @var string
   */
  public const ID = 'id';

  /**
   * Constant for the provider property of a Preprocessor definition.
   *
   * @var string
   */
  public const PROVIDER = 'provider';

  /**
   * Constant for the hooks property of a Preprocessor definition.
   *
   * @var string
   */
  public const HOOKS = 'hooks';

  /**
   * Constant for the themes property of a Preprocessor definition.
   *
   * Only relevant for preprocessors defined in Modules.
   *
   * @var string
   */
  public const THEMES = 'themes';

  /**
   * Constant for the weight property of a Preprocessor definition.
   *
   * @var string
   */
  public const WEIGHT = 'weight';

  /**
   * Dependency injection create method override.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Dependency Injection container.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) : static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginDefinition[self::ID];
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider(): string {
    return $this->pluginDefinition[self::PROVIDER];
  }

  /**
   * {@inheritdoc}
   */
  public function getHooks(): string {
    return $this->pluginDefinition[self::HOOKS];
  }

  /**
   * {@inheritdoc}
   */
  public function getThemes(): array|string {
    return $this->pluginDefinition[self::THEMES] ?? self::ALL_THEMES_VALUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->pluginDefinition[self::WEIGHT] ?? 0;
  }

  /**
   * Alter the data provided to the Preprocessor.
   *
   * Doc is inherited from core template_process() functions as this function
   * works the exact same way as a regular hook would.
   *
   * {@inheritdoc}
   */
  abstract public function preprocess(array &$variables, string $hook, array $info): void;

}
