<?php

namespace Drupal\preprocessors\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * Defines a Preprocessor item attribute object.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class Preprocessor extends Plugin {

  /**
   * Constructs a Preprocessor attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param array $hooks
   *   The target theme hook suggestions to preprocess variables for.
   *   The value here is what you would replace HOOK with in a hook_preprocess_HOOK function.
   *   e.g. "node", "node__basic_page"
   * @param array|string $themes
   *   The themes to preprocess variables in.
   *   Use the value "*" to indicate that the preprocessor works on all themes.
   * @param int $weight
   *   The weight of this preprocessor.
   *   Affects the loading order when multiple preprocessors run on the same hook.
   */
  public function __construct(
    public readonly string $id,
    public readonly array $hooks,
    public readonly array|string $themes = "*",
    public readonly int $weight = 0,
    public readonly ?string $deriver = NULL,
  ) {}

}
